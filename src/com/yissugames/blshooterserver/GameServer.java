package com.yissugames.blshooterserver;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import com.yissugames.blocklife.gamelogic.Block;
import com.yissugames.blocklife.gamelogic.Chunk;
import com.yissugames.blocklife.gamelogic.DirtBlock;
import com.yissugames.blocklife.gamelogic.GrassBlock;
import com.yissugames.blocklife.gamelogic.Inventory.StackInfo;
import com.yissugames.blocklife.gamelogic.LeavesBlock;
import com.yissugames.blocklife.gamelogic.PlankBlock;
import com.yissugames.blocklife.gamelogic.SerializableWorld;
import com.yissugames.blocklife.gamelogic.StoneBlock;
import com.yissugames.blocklife.gamelogic.WoodBlock;
import com.yissugames.blocklife.gamelogic.World;
import com.yissugames.blocklife.server.AddBlock;
import com.yissugames.blocklife.server.BeginWorld;
import com.yissugames.blocklife.server.ChatPackage;
import com.yissugames.blocklife.server.ConnectRequest;
import com.yissugames.blocklife.server.ConnectResponse;
import com.yissugames.blocklife.server.DestroyBlock;
import com.yissugames.blocklife.server.EndWorld;

public class GameServer {

	public static Random random = new Random();
	public static Server server;
	public static World world;
	
	public static ArrayList<ConnectionInformation> connections = new ArrayList<ConnectionInformation>();
	
	public static class ConnectionInformation {
		public int connectionId;
		public String username;
		
		public ConnectionInformation(int connectionId, String username) {
			this.connectionId = connectionId;
			this.username = username;
		}
	}
	
	public static void main(String[] args) {
		
		server = new Server(6666666, 6666666);
		Kryo kryo = server.getKryo();
		kryo.register(int.class);
		
		kryo.register(ConnectRequest.class);
		kryo.register(ConnectResponse.class);
		kryo.register(Chunk.class);
		kryo.register(Block.class);
		kryo.register(Block[].class);
		kryo.register(Block[][].class);
		kryo.register(DirtBlock.class);
		kryo.register(GrassBlock.class);
		kryo.register(LeavesBlock.class);
		kryo.register(PlankBlock.class);
		kryo.register(StoneBlock.class);
		kryo.register(WoodBlock.class);
		
		kryo.register(BeginWorld.class);
		kryo.register(EndWorld.class);
		
		kryo.register(DestroyBlock.class);
		kryo.register(AddBlock.class);
		
		kryo.register(ChatPackage.class);
		
		System.out.println("Starting server...");
		
		Block.LoadBlocks();
		File worldfile = new File(System.getProperty("user.dir") + "\\world.blfile");
		if(!worldfile.exists()) {
			System.out.println("Generating world...");
			world = new World("" + System.currentTimeMillis(), 40);
		} else {
			world = World.loadStatic(System.getProperty("user.dir") + "\\world.blfile");
		}
		
		server.start();
		try {
			server.bind(6999, 6998);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		//---- World Update Thread ----//
		new Thread(new Runnable() {
			@Override
			public void run() {
				world.update();
			}
		}).start();
		
		server.addListener(new Listener() {
			public void received (Connection connection, Object object) {
				if(object instanceof ConnectRequest) {
					char[] array = new char[20];
					for(int i = 0; i < array.length; i++) {
					   array[i] = (char) (random.nextInt(26) + (int) 'a');
					}
					
					ConnectRequest in = (ConnectRequest) object;
					connections.add(new GameServer.ConnectionInformation(connection.getID(), in.username));
					
					ConnectResponse out = new ConnectResponse(new String(array));
					connection.sendTCP(out);
					
					server.sendToAllTCP(new ChatPackage("Server", in.username + " joined the server."));
					
					connection.sendTCP(new BeginWorld(40));
					SerializableWorld w = new SerializableWorld(world);
					for(Chunk c: w.chunks)
						connection.sendTCP(c);
					connection.sendTCP(new EndWorld());
				}
				
				if(object instanceof DestroyBlock) {
					DestroyBlock request = (DestroyBlock) object;
					world.get(request.chunk).destroy(request.x, request.y);
					server.sendToAllExceptTCP(connection.getID(), request);
				}
				
				if(object instanceof AddBlock) {
					AddBlock request = (AddBlock) object;
					world.get(request.chunk).addBlock(request.x, request.y, request.block);
					server.sendToAllExceptTCP(connection.getID(), request);
				}
				
				if(object instanceof ChatPackage) {
					server.sendToAllUDP((ChatPackage) object);
				}
			}
		});
		System.out.println("Server is running.");
		
	}
	
}
